import {
	Component, ViewChild, AfterViewInit, ElementRef, ChangeDetectionStrategy, ChangeDetectorRef
} from '@angular/core';
import cv from 'src/assets/scripts/opencv';

import { WindowRefService } from './services/window-ref.service';


@Component({
	selector: 'app-root',
	changeDetection: ChangeDetectionStrategy.OnPush,
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.sass']
})
export class AppComponent implements AfterViewInit {
	public readonly height = 200;
	public readonly width = 200;
	public src = new cv.Mat(this.height, 100, cv.CV_8UC4);
	public dst = new cv.Mat(this.height, 100, cv.CV_8UC1);

	private readonly navigator = this.winRef.windowRef.navigator;
	private readonly FPS = 30;
	@ViewChild('videoInput') private vidIn: ElementRef<HTMLVideoElement>;
	@ViewChild('videoOut') private vidOut: ElementRef<HTMLCanvasElement>;
	constructor(
		private winRef: WindowRefService,
		private cdr: ChangeDetectorRef
	) { }

	ngAfterViewInit(): void {
		console.log(this.vidIn);
		this.initVideoStream();
	}

	public initVideoStream(): void {
		if (this.navigator.mediaDevices && this.navigator.mediaDevices.getUserMedia) {
			this.navigator.mediaDevices
				.getUserMedia({ video: true, audio: false })
				.then(stream => this.vidIn.nativeElement.srcObject = stream)
				.then(this.playVideoStream)
				.then(() => setTimeout(this.processVideo, 0))
				.catch(console.error);
		}
	}
	public playVideoStream(): void {
		this.vidIn.nativeElement.play();
	}
	public pauseVideoStream(): void {
		this.vidIn.nativeElement.pause();
	}
	public processVideo(): void {
		const context = this.vidOut.nativeElement.getContext('2d');
		const begin = Date.now();

		context.drawImage(this.vidIn.nativeElement, 0, 0, this.width, this.height);
		this.src.data.set(context.getImageData(0, 0, this.width, this.height).data);
		cv.cvtColor(this.src, this.dst, cv.COLOR_RGBA2GRAY);
		cv.imshow('canvasOutput', this.dst);

		const delay = 1000 / this.FPS - (Date.now() - begin);
		setTimeout(this.processVideo, delay);
	}
}
