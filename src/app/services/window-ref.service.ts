import { Injectable } from '@angular/core';


@Injectable({ providedIn: 'root' })
export class WindowRefService {
	private readonly winRef = window;
	constructor() { }

	public get windowRef(): Window {
		return this.winRef;
	}
}
